/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
   
    // joystick
    public static int JOYSTICK_1 = 0;

    // Chassis motors (0~3)
    public static int CHASSIS_LEFT_MOTOR_A = 2;
    public static int CHASSIS_LEFT_MOTOR_B = 3;
    public static int CHASSIS_RIGHT_MOTOR_A = 0;
    public static int CHASSIS_RIGHT_MOTOR_B = 1;
    
    // Elevator motors (4,5)
    public static int ELEVATOR_MOTOR_A = 4;
    public static int ELEVATOR_MOTOR_B = 5;

    // Intaker motors (6 for cargo, 7 for panel)
    public static int PANEL_INTAKER_MOTOR = 6;
    public static int CARGO_INTAKER_MOTOR = 7;

    // PCM 
    public static int PCM_COMPRASSOR = 10;
    public static int SOLENOID_FRONT_FORWARD = 4;
    public static int SOLENOID_FRONT_REVERSE = 5;
    public static int SOLENOID_BACK_FORWARD = 6;
    public static int SOLENOID_BACK_REVERSE = 7;
    

}
