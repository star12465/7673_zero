package frc.robot.subsystems;

import frc.robot.RobotMap;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;


public class IntakerGroup extends Subsystem {
    private WPI_VictorSPX HatchPanelIntaker;
	private WPI_VictorSPX CargoIntaker;
	
    // constructor 
    public IntakerGroup() {
    	HatchPanelIntaker = new WPI_VictorSPX(RobotMap.PANEL_INTAKER_MOTOR);
    	CargoIntaker = new WPI_VictorSPX(RobotMap.CARGO_INTAKER_MOTOR);

    }
    
    public void initDefaultCommand() {
        // pass
    }

    public void releasePanel(double pwd) {
    	HatchPanelIntaker.set(pwd);
    }
    
    public void accuirePanel(double pwd) {
        HatchPanelIntaker.set(-pwd);
    }

    public void stopPanelIntaker() {
        HatchPanelIntaker.set(0);
    }
    public void collectCargo(double pwd) {
        CargoIntaker.set(-pwd);
    }
    
    public void ejectCargo(double pwd) {
        CargoIntaker.set(pwd);
    }

    public void stopCargoIntaker() {
        CargoIntaker.set(0);
    }

}