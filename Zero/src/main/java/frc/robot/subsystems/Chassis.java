package frc.robot.subsystems;

import frc.robot.RobotMap;
import frc.robot.commands.DriveByJoystick;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;


public class Chassis extends Subsystem {
    private WPI_VictorSPX MotorLeftA;
	private WPI_VictorSPX MotorLeftB;
	private WPI_VictorSPX MotorRightA;
	private WPI_VictorSPX MotorRightB;
	
	private SpeedControllerGroup Left;
	private SpeedControllerGroup Right;
	
    private DifferentialDrive Drive;

    // constructor 
    public Chassis() {
        MotorLeftA = new WPI_VictorSPX(RobotMap.CHASSIS_LEFT_MOTOR_A);
    	MotorLeftB = new WPI_VictorSPX(RobotMap.CHASSIS_LEFT_MOTOR_B);
    	MotorRightA = new WPI_VictorSPX(RobotMap.CHASSIS_RIGHT_MOTOR_A);
    	MotorRightB = new WPI_VictorSPX(RobotMap.CHASSIS_RIGHT_MOTOR_B);

    	Left = new SpeedControllerGroup(MotorLeftA, MotorLeftB);
    	Right = new SpeedControllerGroup(MotorRightA, MotorRightB);

		Drive = new DifferentialDrive(Left, Right);

    }
    
    public void initDefaultCommand() {
		setDefaultCommand(new DriveByJoystick());
    }

    public void driveByJotstick(double leftPower, double rightPower) {
    	Drive.tankDrive(leftPower, rightPower);
	}

	public void driveByJotstickArc(double x, double y) {
		Drive.arcadeDrive(x, y);
	}
}