package frc.robot.subsystems;

import frc.robot.RobotMap;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;


public class Elevator extends Subsystem {
    private WPI_VictorSPX MotorA;
	private WPI_VictorSPX MotorB;
	
	private SpeedControllerGroup MotorAB;

    // constructor 
    public Elevator() {
    	MotorA = new WPI_VictorSPX(RobotMap.ELEVATOR_MOTOR_A);
    	MotorB = new WPI_VictorSPX(RobotMap.ELEVATOR_MOTOR_B);
    	MotorAB = new SpeedControllerGroup(MotorA, MotorB);

    }
    
    public void initDefaultCommand() {

    }

    public void up(double pwd) {
    	MotorAB.set(pwd);
    }
    
    public void down(double pwd) {
        MotorAB.set(-pwd);
    }

    public void stop() {
        MotorAB.set(0.2);
    }
}