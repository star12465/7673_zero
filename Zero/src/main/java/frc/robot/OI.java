/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import frc.robot.commands.*;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
    public Joystick stick = new Joystick(RobotMap.JOYSTICK_1);
    Button LB = new JoystickButton(stick, 5),
           RB = new JoystickButton(stick, 6),
           Y = new JoystickButton(stick, 4),
           A = new JoystickButton(stick, 1);
    
    public OI() {
        LB.whenPressed(new ElevatorUp(0.55));
        LB.whenReleased(new ElevatorHold());
        RB.whenReleased(new ElevatorDown(0.1));
        RB.whenReleased(new ElevatorHold());
        Y.whenPressed(new CollectCargo(1));
        A.whenReleased(new EjectCargo(1));
        
    }
}
